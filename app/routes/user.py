from fastapi import APIRouter
from ..src import User

user = User()
router = APIRouter(
    prefix='/user',
    tags=['user'],
    responses={404: {'message': 'Not Found'}}
)


@router.get('/')
def get():
    return user.get()
