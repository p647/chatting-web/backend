from fastapi import APIRouter
from ..src import Board

board = Board()
router = APIRouter(
    prefix='/board',
    tags=['board'],
    responses={404: {'message': 'Not Found'}}
)


@router.get('/')
def get():
    return board.get()


@router.post('/')
def store():
    return board.store()
