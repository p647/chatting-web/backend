from fastapi import FastAPI
from .routes import *

app = FastAPI()
app.include_router(user_router)
app.include_router(board_router)


@app.get("/")
async def root():
    return {"message": "Hello Fast API!"}
